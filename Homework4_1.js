let firstNumber, secondNumber, mathOperator;
function calcUserData(firstNumber, secondNumber, mathOperator) {
    do {
        firstNumber = +prompt(`Enter first number:`, firstNumber)
    } while (firstNumber == undefined || isNaN(firstNumber))
    do {
        secondNumber = +prompt(`Enter second number:`, secondNumber)
    } while (secondNumber == undefined || isNaN(secondNumber))
    do {
        mathOperator = prompt(`Enter the symbol of the desired operation (+, -, *, /)`, mathOperator)
    } while (mathOperator != `/` && mathOperator != '*' && mathOperator != `+` && mathOperator != `-`)
    switch (mathOperator) {
        case `*`:
            result = firstNumber * secondNumber
            return result
            break
        case "/":
            result = firstNumber / secondNumber
            return result
            break
        case "+":
            result = firstNumber + secondNumber
            return result
            break
        case "-":
            result = firstNumber - secondNumber
            return result
            break
    }
}
let userResult1 = calcUserData();
console.log(userResult1)

